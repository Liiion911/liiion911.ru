<div id="leftmenu" class="block">
	<div class="dtop">&nbsp;</div>
	<div class="dcont">
		<div class="btl">
			<h4>Навигация</h4>
		</div>
		<div class="bluemenu">
			<ul class="lmenu reset">
				<li><a href="#">1</a></li>
			</ul>
		</div>
		<div class="dpad">
			<ul class="lmenu reset">
				<li><a href="#">2</a></li>
			</ul>
		</div>
	</div>
	<div class="dbtm">&nbsp;</div>
</div>

<div id="news-arch" class="block">
	<div class="dtop">&nbsp;</div>
	<div class="dcont">
		<div class="btl">
			<h4>Архив новостей</h4>
			<span class="tabmenu">
				<a class="thide tabcal" href="#tabln1">В виде календаря</a>
				<a class="thide tabarh" href="#tabln2">В виде списка</a>
			</span>
		</div>
		<div class="tabcont" id="tabln1">
			<p>В виде календаря</p>
			<div class="dpad">{calendar}</div>
		</div>
		<div class="tabcont" id="tabln2">
			<p>В виде списка</p>
			<div class="dpad">{archives}</div>
		</div>
	</div>
	<div class="dbtm">&nbsp;</div>
</div>

<div id="popular" class="block">
	<div class="dtop">&nbsp;</div>
	<div class="dcont">
		<div class="btl">
			<h4>Популярные статьи</h4>
		</div>
		<ul>
			{topnews}
		</ul>
	</div>
	<div class="dbtm">&nbsp;</div>
</div>

{vote}