
[not-group=5]
	<ul class="reset loginbox">
		<li class="loginava">
			<a href="{profile-link}">
				<img src="{foto}" alt="{login}" />
				<b>&nbsp;</b>
			</a>
		</li>
		<li class="loginbtn">
			<a class="lbn" id="logbtn" href="#"><b>{login}</b></a>
			<a class="thide lexit" href="{logout-link}">Выход</a>
				<div id="logform" class="radial">
					<ul class="reset loginenter">
		[admin-link]<li><a href="{admin-link}" target="_blank"><b>Админпанель</b></a></li>[/admin-link]
					<li><a href="{profile-link}">Мой профиль</a></li>
					<li><a href="{favorites-link}">Мои закладки ({favorite-count})</a></li>
					<li><a href="{newposts-link}">Непрочитанное</a></li>
					<li><a href="?do=lastcomments">Последние комментарии</a></li>
					<li><a href="{stats-link}">Статистика</a></li>
				</ul>
			</div>
		</li>
		<li class="lvsep"><a href="{addnews-link}">Добавить новость</a></li>
		<li class="lvsep"><a class="radial" href="{pm-link}">{new-pm}</a><a href="{pm-link}">Сообщений</a></li>
	</ul>
[/not-group]
<!--
[group=5]
	<ul class="reset loginbox">
		<li class="loginbtn">
			<a class="lbn" id="logbtn" href="#"><b>Войти</b></a>
			<form method="post" action="">
				<div id="logform" class="radial">
					<ul class="reset">
						<li class="lfield"><label for="login_name">{login-method}</label><input type="text" name="login_name" id="login_name" /></li>
						<li class="lfield lfpas"><label for="login_password">Пароль (<a href="{lostpassword-link}">Забыли?</a>):</label><input type="password" name="login_password" id="login_password" /></li>
						<li class="lfield lfchek"><input type="checkbox" name="login_not_save" id="login_not_save" value="1"/><label for="login_not_save">&nbsp;Чужой компьютер</label></li>
						<li class="lbtn"><button class="fbutton" onclick="submit();" type="submit" title="Войти"><span>Войти</span></button></li>
					</ul>
					<input name="login" type="hidden" id="login" value="submit" />
				</div>
			</form>
		</li>
		<li class="lvsep"><a href="{registration-link}">Регистрация</a></li>
	</ul>
[/group]
-->
[group=5]
<script language="javascript" src="engine/modules/vauth/styles/facebox.js"></script>
<link rel="stylesheet" type="text/css" href="engine/modules/vauth/styles/facebox.css" />

<script language="javascript">
	$(document).ready(function() {

		$('a[rel*=facebox]').facebox();

	})

</script>
<link media="screen" type="text/css" rel="stylesheet" href="engine/modules/vauth/styles/vauth_loginform.css"></link>
	
<a class="lbn" id="logbtn" href="#virtual_loginform" rel="facebox"><b>Вход / Регистрация</b></a>
<!--<a class="vauth" href="#virtual_loginform" rel="facebox" >Вход / Регистрация</a>-->


<div style="display:none;" id="virtual_loginform">
<center style="color: #0085cf;font-family: georgia, serif;font-size: 14pt;padding:5px;">Войти на сайт при помощи:</center>
<a class="vkontakte_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=vkontakte"><span class="vk_fl">В</span>контакте</a>
<a class="facebook_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=facebook">facebook</a>
<a class="odnoklassniki_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=odnoklassniki"><span class="odnoklass">одноклассники</span></a>
<a class="twitter_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=twitter"><span class="tw_text_dialog">twitter</span><span class="bird"><img src="engine/modules/vauth/styles/twitter_newbird_blue_small.png" /></span></a>
<a class="google_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=google">Google<b>+</b></a>
<a class="mail_dialog buttons_hover" onclick="ShowLoading('');" href="engine/modules/vauth/auth.php?auth_site=mail">мир<font color="#faa61a">@</font>mail<font color="#faa61a">.ru</font></a>
<img style="float:right;display:block;width:113px;height:41px;margin-top:10px;" src="engine/modules/vauth/styles/vauth_facebox.png" />


<form action="" method="post" class="loginform_vauth">
<input name="login" type="hidden" id="login" value="submit" />
<input class="login_input" name="login_name" type="text" value="Логин" onFocus='if(this.value=="Логин")this.value="";' onBlur='if(this.value=="")this.value="Логин";'/>
<input class="login_input" name="login_password" type="Password" value="Пароль" onFocus='if(this.value=="Пароль")this.value="";' onBlur='if(this.value=="")this.value="Пароль";'/>
<div class="recower"><a href="{lostpassword-link}">Напомнить</a></div><button class="vabutton mybutton" onclick="submit();" type="submit" title="Войти"><span>Войти</span></button><div class="register"><a href="index.php?do=register">Регистрация</a></div>
</form>

</div>
[/group]